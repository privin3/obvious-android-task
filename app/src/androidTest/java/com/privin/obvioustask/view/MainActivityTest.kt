package com.privin.obvioustask.view

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.privin.obvioustask.R
import com.privin.obvioustask.data.model.ImageData
import com.privin.obvioustask.view.detail.ImageDetailFragment
import com.privin.obvioustask.view.home.HomeAdapter
import com.privin.obvioustask.view.home.HomeFragment
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class MainActivityTest {
    @get:Rule
    var activityRule: ActivityTestRule<MainActivity>
            = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testActions() {
        onView(withId(R.id.recycler_view)).perform(
            RecyclerViewActions.actionOnItemAtPosition<HomeAdapter.ImageItemHolder>(1, click())
        )
        onView(withId(R.id.view_pager)).perform(swipeLeft(), swipeRight())

    }
}
