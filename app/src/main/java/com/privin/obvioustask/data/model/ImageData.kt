package com.privin.obvioustask.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImageData (
    @Expose
    @SerializedName("title")
    val title : String?,
    @Expose
    @SerializedName("url")
    val url : String?,
    @Expose
    @SerializedName("hdurl")
    val hdurl : String?,
    @Expose
    @SerializedName("media_type")
    val media_type : String?,
    @Expose
    @SerializedName("service_version")
    val service_version : String?,
    @Expose
    @SerializedName("explanation")
    val explanation : String?,
    @Expose
    @SerializedName("date")
    val date : String?,
    @Expose
    @SerializedName("copyright")
    val copyright : String?) : Parcelable {

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<ImageData> {
            override fun createFromParcel(parcel: Parcel) = ImageData(parcel)
            override fun newArray(size: Int) = arrayOfNulls<ImageData>(size)
        }
    }

    constructor(parcel: Parcel) : this(
        title = parcel.readString(),
        url = parcel.readString(),
        hdurl = parcel.readString(),
        media_type = parcel.readString(),
        service_version = parcel.readString(),
        explanation = parcel.readString(),
        date = parcel.readString(),
        copyright = parcel.readString()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(title)
        dest?.writeString(url)
        dest?.writeString(hdurl)
        dest?.writeString(media_type)
        dest?.writeString(service_version)
        dest?.writeString(explanation)
        dest?.writeString(date)
        dest?.writeString(copyright)
    }

    override fun describeContents(): Int {
        return 0
    }
}