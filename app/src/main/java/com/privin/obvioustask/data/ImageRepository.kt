package com.privin.obvioustask.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.privin.obvioustask.data.model.ImageData
import java.io.InputStream
import java.io.Reader

/*
This is a repository for all image data
 */
class ImageRepository {

    private inline fun <reified T> Gson.fromJson(json: Reader) = this.fromJson<T>(json, object: TypeToken<T>() {}.type)

    /*
    This function reads all data from json file and returns list of ImageData
     */
    fun getAllImages(inputStream: InputStream):List<ImageData>{
        return Gson().fromJson<List<ImageData>>(inputStream.bufferedReader())
    }

}