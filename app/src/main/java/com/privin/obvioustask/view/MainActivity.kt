package com.privin.obvioustask.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.privin.obvioustask.R
import com.privin.obvioustask.util.addFragment
import com.privin.obvioustask.util.replaceFragment
import com.privin.obvioustask.view.detail.ImageDetailFragment
import com.privin.obvioustask.view.detail.ImageViewPagerFragment
import com.privin.obvioustask.view.home.HomeFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Our view container is loaded here
        if (supportFragmentManager.fragments.size==0)
            replaceFragment(HomeFragment(),HomeFragment.TAG)
    }

    override fun onResume() {
        super.onResume()
        setUpToolbar()
    }
    private fun setUpToolbar(){
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        supportActionBar?.title = getString(R.string.app_name)
        // hide toolbar while scrolling content should be disabled
        supportActionBar?.isHideOnContentScrollEnabled = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        /*
        when user is on home screen finishes activity
         */
        for (fragment in supportFragmentManager.fragments){
            if (fragment.tag.equals(HomeFragment.TAG) && fragment.isInLayout){
                finish()
                return
            }
        }
        // reset action bar for the home screen
        setUpToolbar()
        super.onBackPressed()
    }
}
