package com.privin.obvioustask.view.detail

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.privin.obvioustask.data.model.ImageData

class ImageViewPagerAdapter(val list: List<ImageData>,val fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {


    /*
    This function returns detail fragment for each item in list
     */
    override fun getItem(position: Int): Fragment {
        return ImageDetailFragment.newInstance(list[position])
    }

    override fun getCount(): Int {
        return list.size
    }
}