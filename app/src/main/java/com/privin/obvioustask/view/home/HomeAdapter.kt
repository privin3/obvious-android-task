package com.privin.obvioustask.view.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.privin.obvioustask.R
import kotlinx.android.synthetic.main.adapter_image_itemview.view.*

class HomeAdapter(val viewModel: HomeViewModel,val delegate : HomeAdapterDelegate) : RecyclerView.Adapter<HomeAdapter.ImageItemHolder>() {

    interface HomeAdapterDelegate{
        fun onItemClicked(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageItemHolder {
        val inflater = LayoutInflater.from(parent.context)
        val imageItemView = inflater.inflate(R.layout.adapter_image_itemview,parent,false)
        return ImageItemHolder(imageItemView)
    }

    override fun getItemCount(): Int {
        return viewModel.imageDataList.size
    }

    override fun onBindViewHolder(holder: ImageItemHolder, position: Int) {
        // binds data with view
        val data = viewModel.imageDataList[position]
        holder.loadImage(data.url)
        holder.date.text = data.title
        holder.image.setOnClickListener {
            delegate.onItemClicked(position)
        }
    }


    class ImageItemHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val image = itemView.thumbnail_image
        val date = itemView.date

        /*
        This function loads image from url
         */
        fun loadImage(url:String?){
            Glide
                .with(itemView)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(image)
        }
    }
}