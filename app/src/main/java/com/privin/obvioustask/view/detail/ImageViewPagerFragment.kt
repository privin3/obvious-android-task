package com.privin.obvioustask.view.detail


import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager

import com.privin.obvioustask.R
import com.privin.obvioustask.data.model.ImageData
import com.privin.obvioustask.view.MainActivity

/**
 * A simple [Fragment] subclass.
 */
class ImageViewPagerFragment : Fragment() {
    companion object {
        const val TAG = "ImageViewPagerFragment"
        fun newInstance(list: List<ImageData>,position:Int): ImageViewPagerFragment {
            val args = Bundle()
            args.putParcelableArrayList("dataList", list as java.util.ArrayList<out Parcelable>)
            args.putInt("position",position)
            val fragment = ImageViewPagerFragment()
            fragment.arguments = args
            return fragment
        }
    }

    var list = ArrayList<ImageData>()
    var position = 0
    private var activity : MainActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity() as MainActivity
        /*
         passing image list and position as arguments
         */
        arguments?.let{
            list = it.getParcelableArrayList<ImageData>("dataList") as ArrayList<ImageData>
            position = it.getInt("position")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_view_pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewPager = view.findViewById<ViewPager>(R.id.view_pager)

        /*
        sets up adapter and sets current item
         */
        fragmentManager?.let {
            val adapter = ImageViewPagerAdapter(list,it)
            viewPager.adapter = adapter
            viewPager.currentItem = position
        }
    }

    override fun onResume() {
        super.onResume()
        setToolbar()
    }
    private fun setToolbar(){
        activity?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activity?.supportActionBar?.title = ""
        activity?.supportActionBar?.isHideOnContentScrollEnabled = true
    }

}
