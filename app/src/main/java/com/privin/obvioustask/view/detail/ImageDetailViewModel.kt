package com.privin.obvioustask.view.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.privin.obvioustask.data.model.ImageData
import com.privin.obvioustask.util.Tools

class ImageDetailViewModel : ViewModel(){
    var _data = MutableLiveData<ImageData>()
    val data : LiveData<ImageData>
        get() = _data

    fun setImageData(d: ImageData){
        _data.value = d
        println("in viewModel "+ _data.value)
    }

    fun getFormattedDate() : String{
        return Tools.getFormattedDate(data.value?.date)
    }
}