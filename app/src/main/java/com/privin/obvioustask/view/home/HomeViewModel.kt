package com.privin.obvioustask.view.home

import androidx.lifecycle.ViewModel
import com.privin.obvioustask.data.ImageRepository
import com.privin.obvioustask.data.model.ImageData
import com.privin.obvioustask.util.Tools
import java.io.InputStream

/*
view model for [HomeFragment]
 */
open class HomeViewModel : ViewModel() {
    private var _imageDataList  : MutableList<ImageData> = ArrayList()
    val imageDataList : List<ImageData>
        get() = _imageDataList
    private var repo : ImageRepository = ImageRepository()

    /*
    This function loads data from repository
     */
    fun loadImageDataList(inputStream: InputStream){
        _imageDataList.addAll(sortDescending(repo.getAllImages(inputStream)))
    }

    /*
    This function sorts data from latest date
     */
    fun sortDescending(list : List<ImageData>) : List<ImageData>{
        val mutableList : MutableList<ImageData> = ArrayList()
        mutableList.addAll(list)
        mutableList.sortWith(compareByDescending { data -> Tools.getDate(data.date) })
        return mutableList
    }
}