package com.privin.obvioustask.view.home


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.privin.obvioustask.R
import com.privin.obvioustask.util.addFragment
import com.privin.obvioustask.util.replaceFragment
import com.privin.obvioustask.view.MainActivity
import com.privin.obvioustask.view.detail.ImageDetailFragment
import com.privin.obvioustask.view.detail.ImageViewPagerFragment

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment(), HomeAdapter.HomeAdapterDelegate {

    companion object{
        const val TAG = "HomeFragment"
    }
    var activity : MainActivity? = null
    lateinit var viewModel : HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity() as MainActivity
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        context?.let {
            viewModel.loadImageDataList(it.assets.open("data.json"))
        }
        setupRecyclerView(view)
    }

    /*
        setup recycler view with grid layout with span 2
     */
    private fun setupRecyclerView(view:View){
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        val adapter = HomeAdapter(viewModel,this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(view.context,2)
    }

    override fun onItemClicked(position: Int) {
        activity?.addFragment(ImageViewPagerFragment.newInstance(viewModel.imageDataList,position),TAG)
    }
}
