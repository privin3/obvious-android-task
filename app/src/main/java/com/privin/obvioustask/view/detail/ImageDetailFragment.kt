package com.privin.obvioustask.view.detail


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide

import com.privin.obvioustask.R
import com.privin.obvioustask.data.model.ImageData
import com.privin.obvioustask.databinding.FragmentImageDetailBinding
import com.privin.obvioustask.view.MainActivity

/**
 * A simple [Fragment] subclass.
 */
class ImageDetailFragment : Fragment() {

    companion object {

        const val TAG = "ImageDetailFragment"
        fun newInstance(data: ImageData): ImageDetailFragment {
            val args = Bundle()
            args.putParcelable("data", data)
            val fragment = ImageDetailFragment()
            fragment.arguments = args
            return fragment
        }
    }
    lateinit var data: ImageData
    lateinit var binding : FragmentImageDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            data = it.getParcelable<ImageData>("data") as ImageData
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // binding viewModel with the fragment
        val viewModel = ViewModelProvider(this).get(ImageDetailViewModel::class.java)
        viewModel.setImageData(data)

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentImageDetailBinding>(inflater,R.layout.fragment_image_detail,container,false).apply{
            this.lifecycleOwner = this@ImageDetailFragment
            this.viewmodel = viewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // load image to view
        loadImage()
    }

    private fun loadImage(){
        Glide
            .with(this)
            .load(data.hdurl)
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(binding.image)
    }

}
