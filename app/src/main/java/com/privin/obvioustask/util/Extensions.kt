package com.privin.obvioustask.util

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.privin.obvioustask.R

/*
class extensions are written to make class dependent functions calls easy
 */

/*
This function replaces fragment stack with one fragment
 */
fun AppCompatActivity.replaceFragment(fragment:Fragment,tag:String?){
    val fragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.replace(R.id.container,fragment,tag)
        .commit()
}

/*
This function adds fragments on stack
 */
fun AppCompatActivity.addFragment(fragment: Fragment,backstackTag:String){
    val fragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.add(R.id.container,fragment)
        .addToBackStack(backstackTag)
        .commit()
}