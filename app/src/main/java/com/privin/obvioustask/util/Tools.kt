package com.privin.obvioustask.util

import java.lang.IllegalArgumentException
import java.text.SimpleDateFormat
import java.util.*

class Tools {
    companion object{
        fun getDate(string: String?): Date?{
            val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            var date : Date? = null
            string?.let{
                try {
                    df.parse(string)?.let {
                        date = it
                    }
                }catch (err : IllegalArgumentException){
                    println(err.localizedMessage)
                }
            }
            return date
        }

        fun getFormattedDate(date :String?): String{
            val dataFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val displayFormat = SimpleDateFormat("dd MMM yy ", Locale.getDefault())
            date?.let {selfDate->
                val tempDate = dataFormat.parse(selfDate)
                tempDate?.let {
                    return displayFormat.format(it)
                }
            }
            return ""
        }
    }
}