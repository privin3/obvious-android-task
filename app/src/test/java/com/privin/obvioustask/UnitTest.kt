package com.privin.obvioustask

import com.privin.obvioustask.data.model.ImageData
import com.privin.obvioustask.util.Tools
import com.privin.obvioustask.view.home.HomeViewModel
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class UnitTest {

    @Mock
    lateinit var homeViewModel : HomeViewModel

    @Test
    fun checkDate() {
        assertEquals(true,Tools.getDate("2019-12-02")?.after(Tools.getDate("2019-12-01")) )
        assertEquals(false,Tools.getDate("2019-12-01")?.after(Tools.getDate("2019-12-01")) )
        assertEquals(false,Tools.getDate("2019-12-01")?.after(Tools.getDate("2019-12-02")) )
    }

    @Test
    fun checkSort(){
        val list : MutableList<ImageData> = ArrayList()
        val result : MutableList<ImageData> = ArrayList()
        for(i in 10..15){
            list.add(makeData(i))
            result.add(makeData(i))
        }
        result.reverse()
        assertEquals(result[0].date, homeViewModel.sortDescending(list)[0].date)
    }

   private fun makeData(i : Int) : ImageData{
        return ImageData("A$i","www","wwwhd","","","description","2019-12-$i","")
    }
}
